# VEGAS Pro 14 for Linux
![Vegas Pro 14 running on Linux](https://i.imgur.com/afIPpAy.png)
This is a bash script made for easy installation of VEGAS Pro 14.0 on GNU/Linux operating systems using Wine and Winetricks. Primarily, this is made for VEGAS Pro 14 but this script can also be modified to install other versions of Vegas Pro as long as it's made around the same decade as 14.

This script has been tested on Arch Linux but it should probably work with most distros. Other Unix-like operating systems such as BSD are not tested and may require some modification.

The Steam version of VEGAS Pro 14.0 is not supported.
## Prerequisites
You will need the following packages to run this script:
- wine
- winetricks
- cabextract
- gnutls (32-bit version)

You are also expected to provide your own executable installer file as this is copyrighted software and I am not authorized to redistribute it.
### Arch Linux and it's derivatives (Manjaro, EndeavourOS, Artix Linux, etc.)
Acquiring the prerequisites can by done by this executing this command:
```
sudo pacman -S wine winetricks lib32-gnutls cabextract
```
You will need to have the "multilib" repository enabled since it is disabled by default on Arch Linux.
### Debian, Ubuntu, and it's derivatives (Linux Mint, Pop!_OS, MX Linux, etc.)
Debian based distros has an outdated version of Wine and winetricks in their repositories and it's highly recommended to acquire the latest versions.
- Wine for Ubuntu: https://wiki.winehq.org/Ubuntu
- Wine for Debian: https://wiki.winehq.org/Debian
- winetricks: https://github.com/Winetricks/winetricks

Other prerequisites can be installed by executing this command:
```
sudo apt-get install gnutls-bin cabextract
```

### openSUSE
If you use Tumbleweed, you can simply just install the prerequisites like this:
```
sudo zypper install wine winetricks cabextract libgnutls30-32bit
```

If you use Leap, it is recommended to acquire the latest version of Wine instead of just going for what's already available in the repositories by default.

You can get the repositories for the latest version of Wine and Winetricks here: https://en.opensuse.org/Wine#Repositories

Once you add the repository for your version of Leap, you can then execute the same command above as you would for Tumbleweed.

## Variables
There are five variables that can be easily modified to suit whatever needs it may be when it comes to installing Vegas Pro:
- `PREFIX` - This is the name of the Wine prefix. It's not crucial to change this even if using a different version of Vegas Pro as long as there isn't already the prefix made with Vegas already installed.
- `SETUP` - This is the filename of the installer that would be used within the same folder as the script. This is set to take user input as they are about to launch the script.
- `VEGASVER` - Needs to be exactly as branded. Magix brands Vegas all uppercase like "VEGAS". Sony brands it like "Vegas". They should end in ".0". Like "VEGAS Pro 14.0" or "Vegas Pro 12.0".
- `VEGASEXE` - The executable file for running the main program. Different versions are similar to each other like "vegas140.exe", "vegas120.exe", and "vegas150.exe" as examples.
- `ROOTVEGASFILES` - The name of the root folder for different versions of Vegas Pro located in Program Files of the "C:" directory. Version 13 and older is "Sony". Version 14 and newer is "VEGAS".

## Tutorials
### Basic Installation
This installation method is where if you do not modify the script:

1. Clone the repostory by this command:
```
git clone https://gitlab.com/gearshot/vegaspro14linux.git
```
2. Place your installer for VEGAS Pro 14.0 in the "vegaspro14linux" directory.

3. Ensure that the script is executable by performing this command:
```
chmod +x vegaspro14linux.sh
```
4. Type in the location of the script and specifiy the name of the installation file to run the script like this:
```
./vegaspro14linux.sh Vegas-Installer.exe
```
Replace `Vegas-Installer.exe` with an installer you plan to use.

5. Follow on screen instructions and go through installation wizards. After the .NET 4 install, select "Restart Later". Make sure you don't install QuickTime with automatic updates enabled. Wait for a little while after the QuickTime installer.

6. Proceed to install VEGAS Pro 14.0.

7. If all goes well, you should see the message in your terminal say `Installation of VEGAS Pro 14.0 complete!`. Activate and launch Vegas for the first time.

### Installing for PlayOnLinux
For added stability to keep Vegas Pro from getting new bugs as Wine gets updated (or to simply avoid those annoying Wine configuration window as you start up Vegas after a Wine update), you can install Vegas Pro 14 for a PlayOnLinux drive that uses a static version of Wine.

Modify the `PREFIX` variable and have it install inside the "PlayOnLinux's virtual drives" folder then run the script using your terminal (Do not use the "Open a shell" option in PlayOnLinux). It should look something like this when you modify the script:
```
PREFIX=~/PlayOnLinux\'s\ virtual\ drives/vegaspro14
```
You can also execute the script with the default `PREFIX` variable but you would have to move the ".vegaspro14" directory found in your Home over into "PlayOnLinux's virtual drives" directory.

After that, go into your PlayOnLinux's configuration window and you should see that the virtual drive has already been added. You can then change the Wine version to a static version of Wine. Then you make a new Shortcut for Vegas Pro 14 selecting vegas140.exe for easy access.

### Installing OFX Plugins

Assuming you did the basic installation method without modifying the variables, simply navigate to the location of the installer for your OFX Plugin then execute it with the `WINEPREFIX=~/.vegaspro14` envrionment variable before the `wine` command. Like this:
```
WINEPREFIX=~/.vegaspro14 wine OFX-Installer.exe
```
If the installer is an MSI file, then you execute it like this:
```
WINEPREFIX=~/.vegaspro14 msiexec /i OFX-Installer.msi
```
If you intend on installing multiple plugins, you can type in the `export` command for the `WINEPREFIX` enviornment variable for your Terminal session to make it last until you close it like this:
```
export WINEPREFIX=~/.vegaspro14
```
Then you simply run OFX installers with just `wine` or `msiexec /i` without the need to type in the prefix everytime until you close your Terminal session.

If you installed Vegas Pro 14 on a PlayOnLinux prefix, then you can either use PlayOnLinux's "Run a .exe file in this virtual drive" button in the "Miscellaneous" tab on PlayOnLinux configuration window for Vegas Pro 14 or you can use the "Open a shell" option which is in the same tab and use `wine` or `msiexec /i` commands.

## Known Issues

*NOTE: This is about issues related to Vegas Pro running on Wine. If there are issues with the script, then you need to refer to the issues tab for this repository.*

Wine isn't perfect so there are certain things on VEGAS Pro 14 that doesn't work or work as well as it does on Windows. These such things include:
- Inability to import and render WMV files
- Inability to import MOV/M4A files (despite QuickTime being installed)
- MP3 files don't work (use WAV or FLAC files instead)
- Icons above the video preview disappear until you hover your mouse over them sometimes
- Rendering may not be as stable as on Windows (you may need to render in pieces, then combine them together either in VEGAS, in a different video editor like Kdenlive, or by command line tools such as cat and ffmpeg)
- Titles and Text animations does not work. The Titles and Text generators may be usuable but it would have to be static. The text also looks a bit jagged and rough.
- Eyedropper for the color picker in OFX effects options turns the screen black

## Troubleshooting

### Vegas Pro 14's downloader unable to download
You need to install a 32-bit package called "gnutls" using your package manager. That would be `sudo pacman -S lib32-gnutls` on Arch Linux for example. This issue occurs on minimalist distros such as Arch Linux, Void Linux, and Gentoo. Most distros that installs a GUI out of the box such as Ubuntu and Linux Mint, would have this package pre-installed already so most users would not see this error.
